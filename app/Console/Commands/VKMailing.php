<?php

namespace App\Console\Commands;

use App\Models\History;
use App\Models\Mailing;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class VKMailing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:vk_mailing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run mailings in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $mailings = $this->getMailings();

        echo 'Mailing found in hour: ' . $mailings->count() . "\n";

        $this->blockDependencies($mailings);

        foreach ($mailings as $mailing) {

            try {
                $client = new Client();

                foreach ($mailing->groups as $group) {

                    $histories = History::where([
                                    ['group_id', $group->id], ['account_id', $mailing->account->id]
                                ])->get();

                    if ($histories->count() > $group->post_per_day) {
                        echo "Continue 1: " . $group->id . " \n";
                        continue;
                    } elseif (!$mailing->subscribe && $group->subscribe) {
                        echo "Continue 2: " . $group->id . " \n";
                        continue;
                    } elseif (!$mailing->repost && $group->repost) {
                        echo "Continue 3: " . $group->id . " \n";
                        continue;
                    } elseif ($mailing->post->has_link && !$group->links_in_post) {
                        echo "Continue 4: " . $group->id . " \n";
                        continue;
                    } elseif ($group->different_posts && in_array($mailing->post->id, $histories->pluck('post_id')->all())) {
                        echo "Continue 5: " . $group->id . " \n";
                        continue;
                    }

                    $history = History::make([
                            'account_id' => $mailing->account->id,
                            'post_id' => $mailing->post->id,
                            'group_id' => $group->id,
                        ]);

                    try {
                        $response = $this->getResponse($client, $group, $mailing);
                        if (isset($response['response'])) {
                            if (isset($response['response']['post_id'])) {
                                $history->response = $response['response']['post_id'];
                            }
                        }

                    } catch (Exception $exception) {
                        echo $exception->getMessage();
                    }  catch (ErrorException $exception) {
                        echo $exception->getMessage();
                    }

                    $history->save();

                    sleep(env('TIME_BETWEEN'));
                }
            } catch (Exception $exception) {
                echo $exception->getMessage();
            }
        }
    }

    /**
     * @param $mailings
     */
    public function blockDependencies($mailings)
    {
        foreach ($mailings as $mailing) {
            $mailing->account->block();
            $mailing->post->block();

            foreach ($mailing->groups as $group) {
                $group->block();
            }
        }
    }

    /**
     * @return mixed
     */
    public function getMailings()
    {
        $now = (Carbon::now());
        $in_hour = (Carbon::now())->minute($now->minute + 59);
        $mailings = Mailing::where('active', '1')->where('active', 1)
            ->whereBetween('started_at', [$now, $in_hour])->get();
        return $mailings;
    }

    /**
     * @param Client $client
     * @param $group
     * @param $mailing
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getResponse(Client $client, $group, $mailing)
    {
        $response = json_decode(($client->request('GET', 'https://api.vk.com/method/wall.post', [
            'query' => [
                'owner_id' => $group->group_id, // '-152696417'
                'message' => $mailing->post->content,
                'from_group' => '0',
                'attachments' => $mailing->post->image, // 'photo535707753_456239019'
                'v' => '5.65',
                'access_token' => $mailing->account->access_token
            ]
        ]))->getBody(), true);
        return $response;
    }
}
