<?php

namespace App\Http\Controllers;

use App\Models\Account;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class AccountsController extends Controller
{

    /**
     * Display a listing of the accounts.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $accounts = Account::paginate(25);

        return view('accounts.index', compact('accounts'));
    }

    /**
     * Toggle blocked value.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function toggleBlocked($id)
    {
        (Account::findOrFail($id))->toggleBlocked();

        return redirect()->route('accounts.account.index');
    }

    /**
     * Show the form for creating a new account.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('accounts.create');
    }

    /**
     * Store a new account in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            Account::create($data);

            return redirect()->route('accounts.account.index')
                             ->with('success_message', 'Account was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified account.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $account = Account::findOrFail($id);

        return view('accounts.show', compact('account'));
    }

    /**
     * Show the form for editing the specified account.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $account = Account::findOrFail($id);
        

        return view('accounts.edit', compact('account'));
    }

    /**
     * Update the specified account in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);

        $account = Account::findOrFail($id);

        if($account->blocked) {
            return redirect()->back()->withErrors(['Заблокированный элемент не может быть изменен или удален!']);
        }

        $account->update($data);

        return redirect()->route('accounts.account.index')
                             ->with('success_message', 'Account was successfully updated!');
    }

    /**
     * Remove the specified account from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $account = Account::findOrFail($id);

        if($account->blocked) {
            return redirect()->back()->withErrors(['Заблокированный элемент не может быть изменен или удален!']);
        }

        $account->delete();

        return redirect()->route('accounts.account.index')
                         ->with('success_message', 'Account was successfully deleted!');
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'nullable|string|min:0|max:45',
            'blocked' => 'required|boolean',
            'account_url' => 'required|string|min:1|max:255',
            'access_token' => 'required|string|min:1|max:255',
     
        ];

        
        $data = $request->validate($rules);

        return $data;
    }

}
