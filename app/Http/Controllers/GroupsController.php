<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;

class GroupsController extends Controller
{

    /**
     * Display a listing of the groups.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $groups = Group::paginate(25);
        return view('groups.index', compact('groups'));
    }

    /**
     * Toggle blocked value.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function toggleBlocked($id)
    {
        (Group::findOrFail($id))->toggleBlocked();

        return redirect()->route('groups.group.index');
    }

    /**
     * Show the form for creating a new group.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $groups = Group::pluck('group_url','id')->all();
        
        return view('groups.create', compact('groups'));
    }

    /**
     * Store a new group in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $data = $this->getData($request);

        Group::create($data);

        return redirect()->route('groups.group.index')
                         ->with('success_message', 'Group was successfully added!');
    }

    /**
     * Display the specified group.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $group = Group::with('group')->findOrFail($id);

        return view('groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified group.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $group = Group::findOrFail($id);

        return view('groups.edit', compact('group'));
    }

    /**
     * Update the specified group in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);

        $group = Group::findOrFail($id);

        if($group->blocked) {
            return redirect()->back()->withErrors(['Заблокированный элемент не может быть изменен или удален!']);
        }

        $group->update($data);

        return redirect()->route('groups.group.index')
                         ->with('success_message', 'Group was successfully updated!');
    }

    /**
     * Remove the specified group from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $group = Group::findOrFail($id);

        if($group->blocked) {
            return redirect()->back()->withErrors(['Заблокированный элемент не может быть изменен или удален!']);
        }

        $group->mailings()->detach($group->mailings);
        $group->delete();

        return redirect()->route('groups.group.index')
                         ->with('success_message', 'Group was successfully deleted!');
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'group_id' => 'required|regex:/-[0-9]+/|min:1|max:45|unique:groups',
            'group_url' => 'required|regex:/https:\/\/vk.com\/.+/|min:1|max:45|unique:groups',
            'subscribe' => 'boolean',
            'post_per_day' => 'required|numeric',
            'links_in_post' => 'boolean',
            'repost' => 'boolean',
            'different_posts' => 'boolean',
            'blocked' => 'required|boolean',
        ];

        $data = $request->validate($rules, [
            'group_id.unique' => 'Группа с таким id уже зарегистрирована!',
            'group_url.unique' => 'Группа с такой ссылкой уже зарегистрирована!',
            'group_id.regex' => 'Неверный формат id группы! (-ххххххххххх)',
            'group_url.regex' => 'Неверный формат ссылки на группу! (https://vk.com/...)'
        ]);

        return $data;
    }

}
