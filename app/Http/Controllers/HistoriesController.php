<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Group;
use App\Models\History;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class HistoriesController extends Controller
{

    /**
     * Display a listing of the histories.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $histories = History::with('account','post','group')->orderBy('created_at', 'DESC')->paginate(25);

        return view('histories.index', compact('histories'));
    }

    /**
     * Show the form for creating a new history.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $accounts = Account::pluck('name','id')->all();
        $posts = Post::pluck('content','id')->all();
        $groups = Group::pluck('group_url','id')->all();
        
        return view('histories.create', compact('accounts','posts','groups'));
    }

    /**
     * Store a new history in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            History::create($data);

            return redirect()->route('histories.history.index')
                             ->with('success_message', 'History was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified history.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $history = History::with('account','post','group')->findOrFail($id);

        return view('histories.show', compact('history'));
    }

    /**
     * Show the form for editing the specified history.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $history = History::findOrFail($id);
        $accounts = Account::pluck('name','id')->all();
$posts = Post::pluck('content','id')->all();
$groups = Group::pluck('group_url','id')->all();

        return view('histories.edit', compact('history','accounts','posts','groups'));
    }

    /**
     * Update the specified history in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $history = History::findOrFail($id);
            $history->update($data);

            return redirect()->route('histories.history.index')
                             ->with('success_message', 'History was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified history from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $history = History::findOrFail($id);
            $history->delete();

            return redirect()->route('histories.history.index')
                             ->with('success_message', 'History was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'account_id' => 'required',
            'post_id' => 'required',
            'group_id' => 'required',
            'response' => 'required|string|min:1|max:191',
     
        ];

        
        $data = $request->validate($rules);

        return $data;
    }
}
