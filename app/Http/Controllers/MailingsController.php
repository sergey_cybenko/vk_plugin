<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Post;
use App\Models\Mailing;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class MailingsController extends Controller
{

    /**
     * Display a listing of the mailings.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $mailings = Mailing::with('account','post')->paginate(25);

        return view('mailings.index', compact('mailings'));
    }

    /**
     * Show the form for creating a new mailing.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $accounts = Account::pluck('name','id')->all();
        $posts = Post::pluck('name','id')->all();
        $groups = Group::pluck('group_url','id')->all();

        return view('mailings.create', compact('accounts','posts', 'groups'));
    }

    /**
     * Store a new mailing in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $data = $this->getData($request);

        (Mailing::create($data))->groups()->attach($data['groups']);

        return redirect()->route('mailings.mailing.index')
                         ->with('success_message', 'Mailing was successfully added!');

    }

    /**
     * Display the specified mailing.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $mailing = Mailing::with('account','post')->findOrFail($id);

        return view('mailings.show', compact('mailing'));
    }

    /**
     * Show the form for editing the specified mailing.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $mailing = Mailing::findOrFail($id);
        $accounts = Account::pluck('name','id')->all();
        $posts = Post::pluck('name','id')->all();
        $groups = Group::pluck('group_url','id')->all();

        return view('mailings.edit', compact('mailing','accounts','posts', 'groups'));
    }

    /**
     * Update the specified mailing in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);

        $mailing = Mailing::findOrFail($id);
        $mailing->update($data);
        $mailing->groups()->detach($mailing->groups);
        $mailing->groups()->attach($data['groups']);

        return redirect()->route('mailings.mailing.index')
                         ->with('success_message', 'Mailing was successfully updated!');

    }

    /**
     * Remove the specified mailing from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $mailing = Mailing::findOrFail($id);
        $mailing->groups()->detach($mailing->groups);
        $mailing->delete();

        return redirect()->route('mailings.mailing.index')
                         ->with('success_message', 'Mailing was successfully deleted!');

    }

    public function toggleActive($id)
    {
        (Mailing::findOrFail($id))->toggleActive();

        return redirect()->route('mailings.mailing.index');
    }

    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'active' => 'boolean',
            'account_id' => 'required',
            'groups' => 'required|array',
            'post_id' => 'required',
            'subscribe' => 'boolean',
            'links_in_post' => 'boolean',
            'repost' => 'boolean',
            'different_posts' => 'boolean',
            'started_at' => 'required|date_format:Y-m-d H:i:s'
        ];

        $data = $request->validate($rules);

        return $data;
    }

}
