<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class PostsController extends Controller
{

    /**
     * Display a listing of the posts.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::paginate(25);

        return view('posts.index', compact('posts'));
    }

    /**
     * Toggle blocked value.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function toggleBlocked($id)
    {
        (Post::findOrFail($id))->toggleBlocked();

        return redirect()->route('posts.post.index');
    }

    /**
     * Show the form for creating a new post.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a new post in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $data = $this->getData($request);

        Post::create($data);

        return redirect()->route('posts.post.index')
                         ->with('success_message', 'Post was successfully added!');

    }

    /**
     * Display the specified post.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified post.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified post in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);

        $post = Post::findOrFail($id);

        if($post->blocked) {
            return redirect()->back()->withErrors(['Заблокированный элемент не может быть изменен или удален!']);
        }

        $post->update($data);

        return redirect()->route('posts.post.index')
                         ->with('success_message', 'Post was successfully updated!');
    }

    /**
     * Remove the specified post from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        if($post->blocked) {
            return redirect()->back()->withErrors(['Заблокированный элемент не может быть изменен или удален!']);
        }

        $post->delete();

        return redirect()->route('posts.post.index')
                         ->with('success_message', 'Post was successfully deleted!');
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'content' => 'required',
            'image' => 'required|string|min:1|max:45',
            'has_link' => 'boolean',
            'blocked' => 'boolean',
        ];

        $data = $request->validate($rules);

        return $data;
    }

}
