<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accounts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * Get the mailings for this models.
     */
    public function mailings()
    {
        return $this->hasMany('App\Models\Mailing','account_id');
    }

    /**
     * Get the histories for this models.
     */
    public function histories()
    {
        return $this->hasMany('App\Models\History','account_id');
    }

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'name',
                  'account_url',
                  'access_token',
                  'blocked',
                  'created_at',
                  'updated_at'
              ];

    /**
     * Set blocked value true.
     */
    public function block()
    {
        $this->blocked = 1;
        $this->save();
    }

    /**
     * Toggle blocked value.
     */
    public function toggleBlocked()
    {
        if ($this->histories->count() == 0 && $this->mailings->count() == 0) {
            $this->blocked = $this->blocked ? 0: 1;
        }

        $this->save();
    }
}
