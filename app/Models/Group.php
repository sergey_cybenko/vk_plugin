<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'group_id',
                  'group_url',
                  'subscribe',
                  'post_per_day',
                  'links_in_post',
                  'repost',
                  'different_posts',
                  'blocked',
                  'created_at',
                  'updated_at'
              ];
    
    /**
     * Get the group for this model.
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Group','group_id');
    }

    /**
     * Get the mailings for this model.
     */
    public function mailings()
    {
        return $this->belongsToMany('App\Models\Mailing', 'mailing_group');
    }

    /**
     * Get the histories for this models.
     */
    public function histories()
    {
        return $this->hasMany('App\Models\History','group_id');
    }

    /**
     * Set blocked value true.
     */
    public function block()
    {
        $this->blocked = 1;
        $this->save();
    }

    /**
     * Toggle blocked value.
     */
    public function toggleBlocked()
    {
        if ($this->histories->count() == 0 && $this->mailings->count() == 0) {
            $this->blocked = $this->blocked ? 0: 1;
        }

        $this->save();
    }
}
