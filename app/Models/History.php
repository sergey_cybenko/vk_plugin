<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'histories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'account_id',
                  'post_id',
                  'group_id',
                  'response',
                  'created_at',
                  'updated_at'
              ];
    
    /**
     * Get the account for this model.
     */
    public function account()
    {
        return $this->belongsTo('App\Models\Account','account_id');
    }

    /**
     * Get the post for this model.
     */
    public function post()
    {
        return $this->belongsTo('App\Models\Post','post_id');
    }

    /**
     * Get the group for this model.
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Group','group_id');
    }

    /**
     * Get the post link.
     */
    public function getPostLink()
    {
//        https://vk.com/club152696417?w=wall-152696417_55%2Fall
        return $this->group->group_url . '?w=wall' . $this->group->group_id . '_' . $this->response . '%2Fall';
    }
}
