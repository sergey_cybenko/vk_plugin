<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mailing extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mailings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = true;


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'name',
                  'active',
                  'account_id',
                  'post_id',
                  'subscribe',
                  'links_in_post',
                  'repost',
                  'created_at',
                  'updated_at',
                  'started_at'
              ];

    /**
     * Get mailing execution time.
     */
    public function getExecutionTime()
    {
        $groups = $this->groups->count() - 1;
        if($groups > 0) {
            return $groups * ENV('TIME_BETWEEN') / 60;
        }

        return 0;
    }

    /**
     * Toggle active value.
     */
    public function toggleActive()
    {
        if($this->active) {
            $this->active = 0;
        } else {
            $this->active = 1;
        }

        $this->save();
    }
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'started_at'
    ];

    /**
     * Get the account for this model.
     */
    public function account()
    {
        return $this->belongsTo('App\Models\Account','account_id');
    }

    /**
     * Get the post for this model.
     */
    public function post()
    {
        return $this->belongsTo('App\Models\Post','post_id');
    }

    /**
     * Get the groups for this model.
     */
    public function groups()
    {
        return $this->belongsToMany('App\Models\Group', 'mailing_group');
    }
}
