<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'name',
                  'content',
                  'image',
                  'has_link',
                  'blocked',
                  'created_at',
                  'updated_at'
              ];

    /**
     * Get the mailings for this models.
     */
    public function mailings()
    {
        return $this->hasMany('App\Models\Mailing','post_id');
    }

    /**
     * Get the histories for this models.
     */
    public function histories()
    {
        return $this->hasMany('App\Models\History','post_id');
    }

    /**
     * Set blocked value true.
     */
    public function block()
    {
        $this->blocked = 1;
        $this->save();
    }

    /**
     * Toggle blocked value.
     */
    public function toggleBlocked()
    {
        if ($this->histories->count() == 0 && $this->mailings->count() == 0) {
            $this->blocked = $this->blocked ? 0: 1;
        }

        $this->save();
    }
}
