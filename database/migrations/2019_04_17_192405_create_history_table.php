<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('histories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('account_id')->unsigned();
			$table->integer('post_id')->unsigned();
			$table->integer('group_id')->unsigned();
			$table->string('response')->nullable();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});

        Schema::table('histories', function(Blueprint $table) {
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('RESTRICT');
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('RESTRICT');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('RESTRICT');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('history');
	}

}
