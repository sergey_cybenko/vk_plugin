<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateMailingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mailings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('active')->nullable();
			$table->string('name', 255);
			$table->integer('account_id')->unsigned();
			$table->integer('post_id')->unsigned();
            $table->boolean('subscribe');
            $table->boolean('links_in_post');
            $table->boolean('repost');
			$table->timestamp('started_at')->nullable();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});

        Schema::table('mailings', function(Blueprint $table) {
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('RESTRICT');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('RESTRICT');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mailing');
	}

}
