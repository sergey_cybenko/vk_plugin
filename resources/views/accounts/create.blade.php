@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4 class="mt-5 mb-5">Создать Новый Аккаунт</h4>
            </span>
            <a target="_blank" href="https://oauth.vk.com/authorize?client_id=6898826&scope=photos,notes,pages,wall,groups,offline,pages&response_type=token">Сгенерировать токен!</a>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('accounts.account.store') }}" accept-charset="UTF-8" id="create_account_form" name="create_account_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('accounts.form', [
                                        'account' => null,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Добавить">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection


