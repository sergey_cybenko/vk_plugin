
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="col-md-2 control-label">Название</label>
    <div class="col-md-10">
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', optional($account)->name) }}" maxlength="45" placeholder="Enter name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('account_url') ? 'has-error' : '' }}">
    <label for="account_url" class="col-md-2 control-label">Ссылка</label>
    <div class="col-md-10">
        <input class="form-control" name="account_url" type="text" id="account_url" value="{{ old('account_url', optional($account)->account_url) }}" minlength="1" maxlength="191" required="true" placeholder="Enter account url here...">
        {!! $errors->first('account_url', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('access_token') ? 'has-error' : '' }}">
    <label for="access_token" class="col-md-2 control-label">Токен</label>
    <div class="col-md-10">
        <input class="form-control" name="access_token" type="text" id="access_token" value="{{ old('access_token', optional($account)->access_token) }}" minlength="1" maxlength="191" required="true" placeholder="Enter access token here...">
        {!! $errors->first('access_token', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<input type="hidden" value="{{ optional($account)->blocked ? '1': '0' }}" name="blocked">
