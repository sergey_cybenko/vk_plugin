@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Аккаунты</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                @include('layouts.buttons.create', [
                                                        'route' => 'accounts.account.create',
                                                        'title' => 'Добавить новый аккаунт'
                                                    ])
            </div>

        </div>
        
        @if(count($accounts) == 0)
            <div class="panel-body text-center">
                <h4>No Аккаунтs Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Ссылка</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $account)
                        <tr>
                            <td>{{ $account->name }}</td>
                            <td>{{ $account->account_url }}</td>
                            <td>

                                <form method="POST" action="{!! route('accounts.account.destroy', $account->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        @include('layouts.buttons.toggle_blocked', [
                                                                           'id' => $account->id,
                                                                           'route' => 'accounts.account.toggle_blocked',
                                                                           'bool' => $account->blocked,
                                                                           'title' => $account->blocked ? 'Разрешить изменение': 'Запретить изменение'
                                                                        ])
                                        @include('layouts.buttons.show', [
                                                                           'route' => 'accounts.account.show',
                                                                           'id' => $account->id,
                                                                           'title' => 'Показать аккаунт'
                                                                        ])
                                        @include('layouts.buttons.edit', [
                                                                           'route' => 'accounts.account.edit',
                                                                           'id' => $account->id,
                                                                           'title' => 'Изменить аккаунт'
                                                                        ])
                                        @include('layouts.buttons.delete', [
                                                                           'blocked' => $account->blocked,
                                                                           'answer' => 'Удалить аккаунт?',
                                                                           'title' => 'Удалить аккаунт'
                                                                        ])
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $accounts->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection