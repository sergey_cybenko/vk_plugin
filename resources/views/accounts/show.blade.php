@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">Аккаунт</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('accounts.account.destroy', $account->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    @include('layouts.buttons.edit', [
                                                       'route' => 'accounts.account.edit',
                                                       'id' => $account->id,
                                                       'title' => 'Изменить акканунт'
                                                    ])
                    @include('layouts.buttons.delete', [
                                                       'answer' => 'Удалить акканунт?',
                                                       'title' => 'Удалить акканунт'
                                                    ])
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <dl class="dl-horizontal">
            <dt>Название</dt>
            <dd>{{ $account->name }}</dd>
            <dt>Ссылка</dt>
            <dd>{{ $account->account_url }}</dd>
            <dt>Создан</dt>
            <dd>{{ $account->created_at->format('Y-m-d H:i') }}</dd>
            <dt>Обновлен</dt>
            <dd>{{ $account->updated_at->format('Y-m-d H:i') }}</dd>

        </dl>

    </div>
</div>

@endsection