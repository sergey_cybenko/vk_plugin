
<div class="form-group {{ $errors->has('group_id') ? 'has-error' : '' }}">
    <label for="group_id" class="col-md-2 control-label">id Группы</label>
    <div class="col-md-10">
        <input class="form-control" name="group_id" type="text" id="group_id" value="{{ old('group_id', optional($group)->group_id) }}" minlength="1" maxlength="45" required="true" placeholder="Enter group url here...">
        {!! $errors->first('group_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('group_url') ? 'has-error' : '' }}">
    <label for="group_url" class="col-md-2 control-label">Ссылка</label>
    <div class="col-md-10">
        <input class="form-control" name="group_url" type="text" id="group_url" value="{{ old('group_url', optional($group)->group_url) }}" minlength="1" maxlength="45" required="true" placeholder="Enter group url here...">
        {!! $errors->first('group_url', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('subscribe') ? 'has-error' : '' }}">
    <label for="subscribe" class="col-md-2 control-label">Подписка</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="subscribe_1">
                <input name="subscribe" type="hidden" value="0">
            	<input id="subscribe_1" class="" name="subscribe" type="checkbox" value="1" {{ old('subscribe', optional($group)->subscribe) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('subscribe', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('post_per_day') ? 'has-error' : '' }}">
    <label for="post_per_day" class="col-md-2 control-label">Постов в день</label>
    <div class="col-md-10">
        <input class="form-control" name="post_per_day" type="number" id="post_per_day"
               value="{{ old('post_per_day', optional($group)->post_per_day) }}" min="1" max="10"
               placeholder="Enter post_per_day here...">
        {!! $errors->first('post_per_day', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('links_in_post') ? 'has-error' : '' }}">
    <label for="links_in_post" class="col-md-2 control-label">Ссылка в посте</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="links_in_post_1">
                <input name="links_in_post" type="hidden" value="0">
            	<input id="links_in_post_1" class="" name="links_in_post" type="checkbox" value="1" {{ old('links_in_post', optional($group)->links_in_post) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('links_in_post', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('repost') ? 'has-error' : '' }}">
    <label for="repost" class="col-md-2 control-label">Репост</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="repost_1">
                <input name="repost" type="hidden" value="0">
            	<input id="repost_1" class="" name="repost" type="checkbox" value="1" {{ old('repost', optional($group)->repost) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('repost', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('different_posts') ? 'has-error' : '' }}">
    <label for="different_posts" class="col-md-2 control-label">Разные посты</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="different_posts_1">
                <input name="different_posts" type="hidden" value="0">
            	<input id="different_posts_1" class="" name="different_posts" type="checkbox" value="1" {{ old('different_posts', optional($group)->different_posts) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('different_posts', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<input type="hidden" value="{{ optional($group)->blocked ? '1': '0' }}" name="blocked">