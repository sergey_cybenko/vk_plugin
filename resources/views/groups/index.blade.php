@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Группы</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                @include('layouts.buttons.create', [
                                                        'route' => 'groups.group.create',
                                                        'title' => 'Создать новую группу'
                                                    ])
            </div>

        </div>
        
        @if(count($groups) == 0)
            <div class="panel-body text-center">
                <h4>Группы пока еще не были созданы!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>id Группы</th>
                            <th>Ссылка</th>
                            <th>Подписка</th>
                            <th>Постов в день</th>
                            <th>Ссылка в посте</th>
                            <th>Репост</th>
                            <th>Разные посты</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($groups as $group)
                        <tr>
                            <td>{{ $group->group_id }}</td>
                            <td>{{ $group->group_url }}</td>
                            <td>{{ ($group->subscribe) ? '+' : '-' }}</td>
                            <td>{{ $group->post_per_day }}</td>
                            <td>{{ ($group->links_in_post) ? '+' : '-' }}</td>
                            <td>{{ ($group->repost) ? '+' : '-' }}</td>
                            <td>{{ ($group->different_posts) ? '+' : '-' }}</td>
                            <td>

                                <form method="POST" action="{!! route('groups.group.destroy', $group->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        @include('layouts.buttons.toggle_blocked', [
                                                                           'id' => $group->id,
                                                                           'route' => 'groups.group.toggle_blocked',
                                                                           'bool' => $group->blocked,
                                                                           'title' => $group->blocked ? 'Разрешить изменение': 'Запретить изменение'
                                                                        ])
                                        @include('layouts.buttons.show', [
                                                                           'route' => 'groups.group.show',
                                                                           'id' => $group->id,
                                                                           'title' => 'Показать группу'
                                                                        ])
                                        @include('layouts.buttons.edit', [
                                                                           'route' => 'groups.group.edit',
                                                                           'id' => $group->id,
                                                                           'title' => 'Изменить группу'
                                                                        ])
                                        @include('layouts.buttons.delete', [
                                                                           'blocked' => $group->blocked,
                                                                           'answer' => 'Удалить группу?',
                                                                           'title' => 'Удалить группу'
                                                                        ])
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $groups->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection