@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Группа' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('groups.group.destroy', $group->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    @include('layouts.buttons.edit', [
                                                       'route' => 'groups.group.edit',
                                                       'id' => $group->id,
                                                       'title' => 'Изменить группу'
                                                    ])
                    @include('layouts.buttons.delete', [
                                                       'answer' => 'Удалить группу?',
                                                       'title' => 'Удалить группу'
                                                    ])
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <dl class="dl-horizontal">
            <dt>id Группы</dt>
            <dd>{{ $group->group_id }}</dd>
            <dt>Ссылка</dt>
            <dd>{{ $group->group_url }}</dd>
            <dt>Подписка</dt>
            <dd>{{ ($group->subscribe) ? '+' : '-' }}</dd>
            <dt>Постов в день</dt>
            <dd>{{ ($group->post_per_day) ? '+' : '-' }}</dd>
            <dt>Ссылка в посте</dt>
            <dd>{{ $group->links_in_post }}</dd>
            <dt>Репост</dt>
            <dd>{{ ($group->repost) ? '+' : '-' }}</dd>
            <dt>Разные посты</dt>
            <dd>{{ ($group->different_posts) ? '+' : '-' }}</dd>
            <dt>Создан</dt>
            <dd>{{ $group->created_at->format('Y-m-d H:i') }}</dd>
            <dt>Обновлен</dt>
            <dd>{{ $group->updated_at->format('Y-m-d H:i') }}</dd>

        </dl>

    </div>
</div>

@endsection