@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">История</h4>
            </div>

        </div>
        
        @if(count($histories) == 0)
            <div class="panel-body text-center">
                <h4>No Histories Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Название аккаунта</th>
                            <th>Название поста</th>
                            <th>Ссылка на группу</th>
                            <th>Ответ сервера</th>
                            <th>Создан</th>
                            <th>Обновлен</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($histories as $history)
                        <tr>
                            <td>
                                <a href="{{ route('accounts.account.show', ['id' => $history->account->id]) }}">
                                    {{ optional($history->account)->name }}
                                </a>
                            </td>
                            <td>
                                @if($history->response)
                                    <a href="{{ $history->getPostLink() }}" target="_blank">
                                        {{ optional($history->post)->name }}
                                    </a>
                                @else
                                    <span class="text-grey">
                                        {{ optional($history->post)->name }}
                                    </span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ optional($history->group)->group_url }}" target="_blank">
                                    {{ optional($history->group)->group_url }}
                                </a>
                            </td>
                            <td>{{ $history->response }}</td>
                            <td>{{ $history->created_at->format('Y-m-d H:i') }}</td>
                            <td>{{ $history->updated_at->format('Y-m-d H:i') }}</td>

                            <td>
                                <form method="POST" action="{!! route('histories.history.destroy', $history->id) !!}" accept-charset="UTF-8">
                                    <input name="_method" value="DELETE" type="hidden">
                                    {{ csrf_field() }}
                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        @include('layouts.buttons.show', [
                                                                           'route' => 'histories.history.show',
                                                                           'id' => $history->id,
                                                                           'title' => 'Показать историю'
                                                                        ])

                                        @include('layouts.buttons.delete', [
                                                                           'answer' => 'Удалить запись?',
                                                                           'title' => 'Удалить запись'
                                                                        ])
                                    </div>
                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $histories->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection