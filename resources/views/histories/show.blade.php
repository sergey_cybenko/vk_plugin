@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'История' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('histories.history.destroy', $history->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Название аккаунта</dt>
            <dd>
                <a href="{{ route('accounts.account.show', ['id' => $history->account->id]) }}">
                    {{ optional($history->account)->name }}
                </a>
            </dd>
            <dt>Название поста</dt>
            <dd>
                @if($history->response)
                    <a href="{{ $history->getPostLink() }}" target="_blank">
                        {{ optional($history->post)->name }}
                    </a>
                @else
                    <span class="text-grey">
                                        {{ optional($history->post)->name }}
                                    </span>
                @endif
            </dd>
            <dt>Ссылка на группу</dt>
            <dd>
                <a href="{{ optional($history->group)->group_url }}" target="_blank">
                    {{ optional($history->group)->group_url }}
                </a>
            </dd>
            <dt>Ответ сервера</dt>
            <dd>{{ $history->response }}</dd>
            <dt>Создан</dt>
            <dd>{{ $history->created_at->format('Y-m-d H:i') }}</dd>
            <dt>Обновлен</dt>
            <dd>{{ $history->updated_at->format('Y-m-d H:i') }}</dd>

        </dl>

    </div>
</div>

@endsection