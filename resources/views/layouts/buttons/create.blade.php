<a href="{{ route($route) }}" class="btn btn-success" title="{{ $title }}">
    <span class="glyphicon glyphicon-plus" aria-hidden="true">Создать</span>
</a>