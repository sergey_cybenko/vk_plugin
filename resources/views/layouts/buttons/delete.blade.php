@if(isset($blocked))
    @if($blocked)
        <button disabled type="submit" class="btn btn-disabled" title="{{ $title }}" onclick="return confirm(&quot;{{ $answer }}&quot;)">
            <span class="glyphicon glyphicon-trash" aria-hidden="true">Удалить</span>
        </button>
    @else
        <button type="submit" class="btn btn-danger" title="{{ $title }}" onclick="return confirm(&quot;{{ $answer }}&quot;)">
            <span class="glyphicon glyphicon-trash" aria-hidden="true">Удалить</span>
        </button>
    @endif
@else
    <button type="submit" class="btn btn-danger" title="{{ $title }}" onclick="return confirm(&quot;{{ $answer }}&quot;)">
        <span class="glyphicon glyphicon-trash" aria-hidden="true">Удалить</span>
    </button>
@endif