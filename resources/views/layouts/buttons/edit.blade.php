<a href="{{ route($route, $id) }}" class="btn btn-dark" title="{{ $title }}">
    <span class="glyphicon glyphicon-pencil" aria-hidden="true">Изменить</span>
</a>