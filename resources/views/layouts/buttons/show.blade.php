<a href="{{ route($route, $id) }}" class="btn btn-dark" title="{{ $title }}">
    <span class="glyphicon glyphicon-open" aria-hidden="true">Показать</span>
</a>