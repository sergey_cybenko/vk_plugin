<a href="{{ route($route, $id) }}" class="btn btn-dark" title="{{ $title }}">
    @if($bool)
        <span class="glyphicon glyphicon-trash" aria-hidden="true">+</span>
    @else
        <span class="glyphicon glyphicon-trash" aria-hidden="true">-</span>
    @endif
</a>