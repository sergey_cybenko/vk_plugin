@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">{{ !empty($title) ? $title : 'Рассылки' }}</h4>
            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('mailings.mailing.update', $mailing->id) }}" id="edit_mailing_form" name="edit_mailing_form" accept-charset="UTF-8" class="form-horizontal">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('mailings.form', [
                                        'mailing' => $mailing,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection