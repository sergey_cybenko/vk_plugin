<div class="form-group {{ $errors->has('active') ? 'has-error' : '' }}">
    <label for="active" class="col-md-2 control-label">Активна</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="active">
                <input name="active" type="hidden" value="0">
                <input name="active" type="checkbox" id="active" value="1" {{ old('active', optional($mailing)->active) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="col-md-2 control-label">Название</label>
    <div class="col-md-10">
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', optional($mailing)->name) }}" maxlength="45" placeholder="Enter name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('account_id') ? 'has-error' : '' }}">
    <label for="account_id" class="col-md-2 control-label">Аккаунт</label>
    <div class="col-md-10">
        <select class="form-control" id="account_id" name="account_id" required="true">
            <option value="" style="display: none;"
                    {{ old('account_id', optional($mailing)->account_id ?: '') == '' ? 'selected' : '' }} disabled
                    selected>Enter account here...
            </option>
            @foreach ($accounts as $key => $account)
                <option value="{{ $key }}" {{ old('account_id', optional($mailing)->account_id) == $key ? 'selected' : '' }}>
                    {{ $account }}
                </option>
            @endforeach
        </select>

        {!! $errors->first('account_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('groups') ? 'has-error' : '' }}">
    <label for="groups" class="col-md-2 control-label">Группы</label>
    <div class="col-md-10">
        <select multiple="multiple" class="form-control" id="groups" name="groups[]" required="true">
            @foreach ($groups as $key => $group)
                @if(is_null(optional($mailing)->groups))
                    <option value="{{ $key }}" {{ old('groups', optional($mailing)->groups) == $key ? 'selected' : '' }}>
                        {{ $group }}
                    </option>
                @else
                    <option value="{{ $key }}" {{ old('groups', in_array($key, optional($mailing)->groups->pluck('id')->all())) ? 'selected' : '' }}>
                        {{ $group }}
                    </option>
                @endif
            @endforeach
        </select>

        {!! $errors->first('account_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('post_id') ? 'has-error' : '' }}">
    <label for="post_id" class="col-md-2 control-label">Пост</label>
    <div class="col-md-10">
        <select class="form-control" id="post_id" name="post_id" required="true">
            <option value="" style="display: none;"
                    {{ old('post_id', optional($mailing)->post_id ?: '') == '' ? 'selected' : '' }} disabled selected>
                Enter post here...
            </option>
            @foreach ($posts as $key => $post)
                <option value="{{ $key }}" {{ old('post_id', optional($mailing)->post_id) == $key ? 'selected' : '' }}>
                    {{ $post }}
                </option>
            @endforeach
        </select>

        {!! $errors->first('post_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('subscribe') ? 'has-error' : '' }}">
    <label for="subscribe" class="col-md-2 control-label">Учитывать группы, требующие подписку</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="subscribe_1">
            	<input name="subscribe" type="hidden" value="0">
            	<input id="subscribe_1" class="" name="subscribe" type="checkbox" value="1" {{ old('subscribe', optional($mailing)->subscribe) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('subscribe', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('links_in_post') ? 'has-error' : '' }}">
    <label for="links_in_post" class="col-md-2 control-label">Учитывать группы, разрешающие ссылки в посте</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="links_in_post_1">
            	<input name="links_in_post" type="hidden" value="0">
            	<input id="links_in_post_1" class="" name="links_in_post" type="checkbox" value="1" {{ old('links_in_post', optional($mailing)->links_in_post) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('links_in_post', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('repost') ? 'has-error' : '' }}">
    <label for="repost" class="col-md-2 control-label">Учитывать группы, обязующие делать репост записи</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="repost_1">
                <input name="repost" type="hidden" value="0">
            	<input id="repost_1" class="" name="repost" type="checkbox" value="1" {{ old('repost', optional($mailing)->repost) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('repost', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <label for="started_at" class="col-md-2 control-label">Начало рассылки</label>
    <div class="col-md-10">
        <div class="input-group date" id="timepicker" data-target-input="nearest">
            @if(isset($mailing))
                <input type="text" class="form-control datetimepicker-input" value="{{ $mailing->started_at->format('Y-m-d H:i:s') }}" data-target="#timepicker" name="started_at" id="started_at" required="true"/>
            @else
                <input type="text" class="form-control datetimepicker-input" data-target="#timepicker" name="started_at" id="started_at" required="true"/>
            @endif
            <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
            </div>
        </div>
        {!! $errors->first('started_at', '<p class="help-block">:message</p>') !!}
    </div>

</div>
<script type="text/javascript">
    $(function () {
        $('#timepicker').datetimepicker({
            locale: 'ru',
            format: 'YYYY-MM-DD HH:mm:ss',
            icons: {
                time: "far fa-clock",
                date: "far fa-calendar-alt",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
        });
    });
</script>

<input type="hidden" value="{{ optional($mailing)->blocked ? '1': '0' }}" name="blocked">
