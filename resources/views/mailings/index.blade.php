@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Рассылки</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                @include('layouts.buttons.create', [
                                                        'route' => 'mailings.mailing.create',
                                                        'title' => 'Создать новую рассылку'
                                                    ])
            </div>

        </div>
        
        @if(count($mailings) == 0)
            <div class="panel-body text-center">
                <h4>No Mailings Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Время выполнения</th>
                            <th>Активна</th>
                            <th>Аккаунт</th>
                            <th>Пост</th>
                            <th>Время запуска</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($mailings as $mailing)
                        <tr>
                            <td>{{ $mailing->getExecutionTime() }}</td>
                            <td>
                                <a href="{{ route('mailings.mailing.toggle_active', $mailing->id) }}" class="btn {{ $mailing->active ? 'btn-dark' : 'btn-secondary' }}" title="{{ $mailing->active ? 'Деактивировать рассылку' : 'Активировать рассылку' }}">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true">{{ $mailing->active ? '+' : '-' }}</span>
                                </a>
                            </td>
                            <td>{{ optional($mailing->account)->name }}</td>
                            <td>{{ optional($mailing->post)->content }}</td>
                            <td>{{ $mailing->started_at->format('Y-m-d H:i') }}</td>
                            <td>
                                <form method="POST" action="{!! route('mailings.mailing.destroy', $mailing->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        @include('layouts.buttons.show', [
                                                                           'route' => 'mailings.mailing.show',
                                                                           'id' => $mailing->id,
                                                                           'title' => 'Показать рассылку'
                                                                        ])
                                        @include('layouts.buttons.edit', [
                                                                           'route' => 'mailings.mailing.edit',
                                                                           'id' => $mailing->id,
                                                                           'title' => 'Изменить рассылку'
                                                                        ])
                                        @include('layouts.buttons.delete', [
                                                                           'answer' => 'Удалить рассылку?',
                                                                           'title' => 'Удалить рассылку'
                                                                        ])
                                    </div>

                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $mailings->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection