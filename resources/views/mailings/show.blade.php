@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <div class="row">
            <div class="col-md-2">
                <span class="pull-left">
                    <h4 class="my-5">{{ isset($title) ? $title : 'Рассылки' }}</h4>
                </span>

            </div>
            <div class="col-md-2">
                <div class="pull-right my-5">

                    <form method="POST" action="{!! route('mailings.mailing.destroy', $mailing->id) !!}" accept-charset="UTF-8">
                        <input name="_method" value="DELETE" type="hidden">
                        {{ csrf_field() }}
                        <div class="btn-group btn-group-sm" role="group">
                            @include('layouts.buttons.edit', [
                                                               'route' => 'mailings.mailing.edit',
                                                               'id' => $mailing->id,
                                                               'title' => 'Изменить рассылку'
                                                            ])
                            @include('layouts.buttons.delete', [
                                                               'answer' => 'Удалить рассылку?',
                                                               'title' => 'Удалить рассылку'
                                                            ])
                        </div>
                    </form>

                </div>
            </div>
            <div class="col-md-8">
                <span class="pull-left">
                    <h4 class="mt-5 mb-5">Группы</h4>
                </span>
            </div>
        </div>


    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-md-2">
                <dl class="dl-horizontal">
                    <dt>Активна</dt>
                    <dd>{{ $mailing->active ? '+' : '-' }}</dd>
                    <dt>Аккаунт</dt>
                    <dd>{{ optional($mailing->account)->name }}</dd>
                    <dt>Пост</dt>
                    <dd>{{ optional($mailing->post)->content }}</dd>
                    <dt>Начало рассылки</dt>
                    <dd>{{ $mailing->started_at->format('Y-m-d H:i') }}</dd>
                    <dt>Учитывать группы, обязующие делать репост записи</dt>
                    <dd>{{ $mailing->repost ? '+' : '-' }}</dd>
                    <dt>Учитывать группы, разрешающие ссылки в посте</dt>
                    <dd>{{ $mailing->links_in_post ? '+' : '-' }}</dd>
                    <dt>Учитывать группы, требующие подписку</dt>
                    <dd>{{ $mailing->repost ? '+' : '-' }}</dd>

                </dl>
            </div>
            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Ссылки</th>
                                <th>Подписка</th>
                                <th>Ссылки в посте</th>
                                <th>Постов в день</th>
                                <th>Репост</th>
                                <th>Заблокирована</th>
                            </tr>

                        </thead>
                        <tbody>
                            @foreach($mailing->groups as $group)
                                <tr>
                                    <td><a href="{{ $group->group_url }}">ссылка</a></td>
                                    <td>{{ $group->subscribe ? '+' : '-' }}</td>
                                    <td>{{ $group->links_in_post ? '+' : '-' }}</td>
                                    <td>{{ $group->post_per_day }}</td>
                                    <td>{{ $group->repost ? '+' : '-' }}</td>
                                    <td>{{ $group->blocked ? '+' : '-' }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>

@endsection