<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="col-md-2 control-label">Название</label>
    <div class="col-md-10">
        <input class="form-control" name="name" type="text" id="name"
               value="{{ old('name', optional($post)->name) }}"
               placeholder="Enter name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
    <label for="content" class="col-md-2 control-label">Контент</label>
    <div class="col-md-10">
        <textarea class="form-control" name="content" cols="50" rows="10" id="content" required="true" placeholder="Enter content here...">{{ old('content', optional($post)->content) }}</textarea>
        {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
    <label for="image" class="col-md-2 control-label">Изображение</label>
    <div class="col-md-10">
        <input class="form-control" name="image" type="text" id="image" value="{{ old('image', optional($post)->image) }}" minlength="1" maxlength="45" required="true" placeholder="Enter image here...">
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('has_link') ? 'has-error' : '' }}">
    <label for="has_link" class="col-md-2 control-label">Есть ссылка</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="has_link_1">
                <input name="has_link" type="hidden" value="0">
            	<input id="has_link_1" class="" name="has_link" type="checkbox" value="1" {{ old('has_link', optional($post)->has_link) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('has_link', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<input type="hidden" value="{{ optional($post)->blocked ? '1': '0' }}" name="blocked">
