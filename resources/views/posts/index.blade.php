@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Посты</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                @include('layouts.buttons.create', [
                                                        'route' => 'posts.post.create',
                                                        'title' => 'Создать новую пост'
                                                    ])
            </div>

        </div>
        
        @if(count($posts) == 0)
            <div class="panel-body text-center">
                <h4>No Posts Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Содержит ссылку</th>
                            <th>Обновлен</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)

                        <tr>
                            <td>{{ $post->name }}</td>
                            <td>{{ $post->has_link ? '+': '-' }}</td>
                            <td>{{ $post->updated_at }}</td>
                            <td>

                                <form method="POST" action="{!! route('posts.post.destroy', $post->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        @include('layouts.buttons.toggle_blocked', [
                                                                           'id' => $post->id,
                                                                           'route' => 'posts.post.toggle_blocked',
                                                                           'bool' => $post->blocked,
                                                                           'title' => $post->blocked ? 'Разрешить изменение': 'Запретить изменение'
                                                                        ])
                                        @include('layouts.buttons.show', [
                                                                           'route' => 'posts.post.show',
                                                                           'id' => $post->id,
                                                                           'title' => 'Показать пост'
                                                                        ])
                                        @include('layouts.buttons.edit', [
                                                                           'route' => 'posts.post.edit',
                                                                           'id' => $post->id,
                                                                           'title' => 'Изменить пост'
                                                                        ])
                                        @include('layouts.buttons.delete', [
                                                                           'blocked' => $post->blocked,
                                                                           'answer' => 'Удалить пост?',
                                                                           'title' => 'Удалить пост'
                                                                        ])
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $posts->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection