@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Пост' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('posts.post.destroy', $post->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    @include('layouts.buttons.edit', [
                                                       'route' => 'posts.post.edit',
                                                       'id' => $post->id,
                                                       'title' => 'Изменить пост'
                                                    ])
                    @include('layouts.buttons.delete', [
                                                       'answer' => 'Удалить пост?',
                                                       'title' => 'Удалить пост'
                                                    ])
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <dl class="dl-horizontal">
            <dt>Контент</dt>
            <dd>{{ $post->content }}</dd>
            <dt>Изображение</dt>
            <dd>{{ $post->image }}</dd>
            <dt>Есть ссылка</dt>
            <dd>{{ ($post->has_link) ? '+' : '-' }}</dd>
            <dt>Заблокирован</dt>
            <dd>{{ ($post->blocked) ? '+' : '-' }}</dd>
            <dt>Создан</dt>
            <dd>{{ $post->created_at->format('Y-m-d H:i') }}</dd>
            <dt>Обновлен</dt>
            <dd>{{ $post->updated_at->format('Y-m-d H:i') }}</dd>

        </dl>

    </div>
</div>

@endsection