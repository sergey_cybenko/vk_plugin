<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware(['auth'])->group(function () {

    Route::get('/home', 'MailingsController@index')->name('home');

    Route::get('/', 'MailingsController@index');

    Route::group(
        [
            'prefix' => 'posts',
        ], function () {

        Route::get('/', 'PostsController@index')
            ->name('posts.post.index');

        Route::get('/toggle-blocked/{post}', 'PostsController@toggleBlocked')
            ->name('posts.post.toggle_blocked');

        Route::get('/create', 'PostsController@create')
            ->name('posts.post.create');

        Route::get('/show/{post}', 'PostsController@show')
            ->name('posts.post.show')
            ->where('id', '[0-9]+');

        Route::get('/{post}/edit', 'PostsController@edit')
            ->name('posts.post.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'PostsController@store')
            ->name('posts.post.store');

        Route::put('post/{post}', 'PostsController@update')
            ->name('posts.post.update')
            ->where('id', '[0-9]+');

        Route::delete('/post/{post}', 'PostsController@destroy')
            ->name('posts.post.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'groups',
        ], function () {

        Route::get('/', 'GroupsController@index')
            ->name('groups.group.index');

        Route::get('/toggle-blocked/{group}', 'GroupsController@toggleBlocked')
            ->name('groups.group.toggle_blocked');

        Route::get('/create', 'GroupsController@create')
            ->name('groups.group.create');

        Route::get('/show/{group}', 'GroupsController@show')
            ->name('groups.group.show')
            ->where('id', '[0-9]+');

        Route::get('/{group}/edit', 'GroupsController@edit')
            ->name('groups.group.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'GroupsController@store')
            ->name('groups.group.store');

        Route::put('group/{group}', 'GroupsController@update')
            ->name('groups.group.update')
            ->where('id', '[0-9]+');

        Route::delete('/group/{group}', 'GroupsController@destroy')
            ->name('groups.group.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'mailings',
        ], function () {

        Route::get('/', 'MailingsController@index')
            ->name('mailings.mailing.index');

        Route::get('/toggle-active/{mailing}', 'MailingsController@toggleActive')
            ->name('mailings.mailing.toggle_active');

        Route::get('/create', 'MailingsController@create')
            ->name('mailings.mailing.create');

        Route::get('/show/{mailing}', 'MailingsController@show')
            ->name('mailings.mailing.show')
            ->where('id', '[0-9]+');

        Route::get('/{mailing}/edit', 'MailingsController@edit')
            ->name('mailings.mailing.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'MailingsController@store')
            ->name('mailings.mailing.store');

        Route::put('mailing/{mailing}', 'MailingsController@update')
            ->name('mailings.mailing.update')
            ->where('id', '[0-9]+');

        Route::delete('/mailing/{mailing}', 'MailingsController@destroy')
            ->name('mailings.mailing.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'accounts',
        ], function () {

        Route::get('/', 'AccountsController@index')
            ->name('accounts.account.index');

        Route::get('/toggle-blocked/{account}', 'AccountsController@toggleBlocked')
            ->name('accounts.account.toggle_blocked');

        Route::get('/create', 'AccountsController@create')
            ->name('accounts.account.create');

        Route::get('/show/{account}', 'AccountsController@show')
            ->name('accounts.account.show')
            ->where('id', '[0-9]+');

        Route::get('/{account}/edit', 'AccountsController@edit')
            ->name('accounts.account.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'AccountsController@store')
            ->name('accounts.account.store');

        Route::put('account/{account}', 'AccountsController@update')
            ->name('accounts.account.update')
            ->where('id', '[0-9]+');

        Route::delete('/account/{account}', 'AccountsController@destroy')
            ->name('accounts.account.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'histories',
        ], function () {

        Route::get('/', 'HistoriesController@index')
            ->name('histories.history.index');

        Route::get('/show/{history}', 'HistoriesController@show')
            ->name('histories.history.show')
            ->where('id', '[0-9]+');

        Route::delete('/history/{history}', 'HistoriesController@destroy')
            ->name('histories.history.destroy')
            ->where('id', '[0-9]+');

    });
});
